<?php

namespace Drupal\broadcast_message\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\Core\Database\Connection;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Provides route responses for the broadcast_message module.
 */
class BroadcastMessageController extends ControllerBase {

  protected $currentUser;
  protected $database;
  protected $renderer;

  /**
   * Constructs a new BroadcastMessageController object.
   */
  public function __construct(AccountInterface $currentUser, Connection $database, RendererInterface $renderer) {
    $this->currentUser = $currentUser;
    $this->database = $database;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'), $container->get('database'), $container->get('renderer')
    );
  }

  /**
   * Returns a page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function broadcastMessageList() {
    $query = $this->database->select('broadcast_message', 'bm');
    $query->fields('bm', ['id', 'message_title']);
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(20);
    $query->orderBy("bm.created", 'DESC');
    $results = $pager->execute()->fetchAll();
    $output = [];
    $header = ['#', 'Title', 'Operations'];
    $i = 1;
    foreach ($results as $result) {
      $operation_drop_button = [[
        '#type' => 'dropbutton',
        '#links' =>
        [
          'edit-page' => ['title' => $this->t('Edit'), 'url' => Url::fromUri('internal:/admin/people/broadcast-message/' . $result->id . '/edit')],
          'delete-page' => ['title' => $this->t('Delete'), 'url' => Url::fromUri('internal:/admin/people/broadcast-message/' . $result->id . '/delete')],
        ],
      ],
      ];
      $operations = $this->renderer->render($operation_drop_button);
      $output[$result->id] = [
        'id' => $i,
        'message_title' => $result->message_title,
        'operations' => $operations,
      ];
      $i++;
    }
    $build['config_table'] = [
      '#theme' => 'table',
      '#header' => $header,
      '#rows' => $output,
    ];
    $build['pager'] = [
      '#type' => 'pager',
    ];
    return $build;
  }

  /**
   * Returns a page.
   *
   * @return array
   *   A simple renderable array.
   */
  public function broadcastMessageListTest() {
    $status = 'true';
    $response['status'] = 'false';
    $current_user = $this->currentUser();
    $username = $current_user->getAccountName();
    $roles = $current_user->getRoles();
    if (isset($_SESSION['first_' . $username]) && ($_SESSION['first_' . $username])) {
      $today_date = date('Y-m-d');
      $query = $this->database->select('broadcast_message', 'bm');
      $query->fields('bm');
      $query->condition('start_date', $today_date, '<=');
      $query->condition('end_date', $today_date, '>=');
      $results = $query->execute()->fetchAll();
      if ($results) {
        foreach ($results as $key => $result) {
          $message_role = unserialize($result->roles);
          $show_status = FALSE;
          foreach ($message_role as $mr) {
            if (in_array($mr, $roles)) {
              $show_status = TRUE;
            }
          }
          if ($show_status) {
            $response['broadcast_message'][$key]['title'] = $result->message_title;
            $response['broadcast_message'][$key]['message'] = $result->message_desc;
            $response['status'] = $status;
          }
        }
      }
      $_SESSION['first_' . $username] = FALSE;
    }
    return new JsonResponse($response);
  }

}
