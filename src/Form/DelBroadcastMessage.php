<?php

namespace Drupal\broadcast_message\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * DelBroadcastMessage form.
 */
class DelBroadcastMessage extends ConfirmFormBase {

  /**
   * The record id.
   *
   * @var int
   */
  protected $id;

  /**
   * Returns the question to ask the user.
   *
   * @return string
   *   The form question. The page title will be set to this value.
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete this broadcast message?');
  }

  /**
   * Returns additional text to display as a description.
   *
   * @return string
   *   Message
   */
  public function getDescription() {
    $db = \Drupal::database();
    $query = $db->select('broadcast_message', 'bm');
    $query->fields('bm', ['id', 'message_title']);
    $query->condition('id', $this->id);
    $results = $query->execute()->fetchAssoc();
    return $this->t('Are you sure you want to delete <i>' . $results['message_title'] . '</i> broadcast message?');
  }

  /**
   * Returns the route to go to if the user cancels the action.
   *
   * @return \Drupal\Core\Url
   *   A URL object.
   */
  public function getCancelUrl() {
    return new Url('broadcast_message.setting');
  }

  /**
   * Returns a unique string identifying the form.
   *
   * @return string
   *   The unique string identifying the form.
   */
  public function getFormId() {
    return 'broadcast_message_delete_confirm_form';
  }

  /**
   * {@inheritdoc}
   *
   * @param int $id
   *   (optional) The ID of the record to be deleted.
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $this->id = $id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the form.
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $db = \Drupal::database();
    $db->delete('broadcast_message')
      ->condition('id', $this->id)
      ->execute();
    $this->messenger()->addStatus(t('Broadcast message has been deleted.'));
    $form_state->setRedirectUrl($this->getCancelUrl());
  }

}
