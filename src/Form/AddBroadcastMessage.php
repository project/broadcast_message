<?php

namespace Drupal\broadcast_message\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Component\Utility\Html;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Database\Connection;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Add Broadcast Message form.
 */
class AddBroadcastMessage extends FormBase {
  protected $currentUser;
  protected $database;

  /**
   * Constructs a new AddBroadcastMessage object.
   */
  public function __construct(AccountInterface $currentUser, Connection $database) {
    $this->currentUser = $currentUser;
    $this->database = $database;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('current_user'), $container->get('database')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'broadcast_message_add_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $id = NULL) {
    $results = [];
    $roles = [];
    $user_role_names = user_role_names();
    unset($user_role_names['anonymous']);
    if (isset($id)) {
      $query = $this->database->select('broadcast_message', 'bm');
      $query->fields('bm');
      $query->condition('id', $id);
      $results = $query->execute()->fetchAll();
      if (!empty($results)) {
        $roles = unserialize($results[0]->roles);
      }
    }
    $this->id = !empty($results) ? $results[0]->id : "";
    $form['message_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#default_value' => !empty($results) ? $results[0]->message_title : '',
      '#required' => TRUE,
    ];
    $form['message_desc'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#rows' => 10,
      '#cols' => 60,
      '#resizable' => FALSE,
      '#default_value' => !empty($results) ? $results[0]->message_desc : '',
      '#required' => TRUE,
    ];
    $form['roles'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Roles'),
      '#options' => $user_role_names,
      '#default_value' => !empty($results) ? $roles : [],
      '#required' => TRUE,
    ];
    $form['start_date'] = [
      '#type' => 'date',
      '#title' => $this->t('Start Date'),
      '#default_value' => !empty($results) ? $results[0]->start_date : '',
      '#required' => TRUE,
    ];
    $form['end_date'] = [
      '#type' => 'date',
      '#title' => $this->t('End Date'),
      '#default_value' => !empty($results) ? $results[0]->end_date : '',
      '#required' => TRUE,
    ];
    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Save'),
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Validation.
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $message_role = [];
    foreach ($form_state->getValue('roles') as $enabled) {
      if ($enabled) {
        $message_role[] = $enabled;
      }
    }
    if (!empty($this->id)) {
      $query = $this->database->update('broadcast_message')
        ->fields([
          'message_title' => Html::escape($form_state->getValue('message_title')),
          'message_desc' => Html::escape($form_state->getValue('message_desc')),
          'roles' => serialize($message_role),
          'start_date' => Html::escape($form_state->getValue('start_date')),
          'end_date' => Html::escape($form_state->getValue('end_date')),
        ])
        ->condition('id', $this->id);
      $query->execute();
    }
    else {
      $query = $this->database->insert('broadcast_message')
        ->fields([
          'message_title' => Html::escape($form_state->getValue('message_title')),
          'message_desc' => Html::escape($form_state->getValue('message_desc')),
          'roles' => serialize($message_role),
          'start_date' => Html::escape($form_state->getValue('start_date')),
          'end_date' => Html::escape($form_state->getValue('end_date')),
          'created' => date('Y-m-d H:i:s'),
        ]);
      $query->execute();
      $t_args = ['@type' => 'Broadcast Message', '%title' => $form_state->getValue('message_title')];
      $this->messenger()->addStatus(strip_tags($this->t('@type %title has been created.', $t_args)));
    }
    $form_state->setRedirect('broadcast_message.setting');
    return;
  }

}
