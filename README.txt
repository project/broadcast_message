CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------




REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------



CONFIGURATION
-------------



MAINTAINERS
-----------

8.x-1.x Developed and maintained by:
 * Manish Jain (https://www.drupal.org/u/manishj)
 * Pradeep Shukla (https://www.drupal.org/user/2957467)
