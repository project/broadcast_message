(function ($) {
  $(document).ready(function () {
    $.ajax({
      url: Drupal.url('admin/people/broadcast-message/message-popup'),
      type: "POST",
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: function (response) {
        var html = '';
        $.each(response.broadcast_message, function (key, value) {
          html += '<strong>' + value.title + '</strong>';
          html += '<p>' + value.message + '</p>';
        });
        if (response.status == 'true') {
          var myDialog = $('<div>' + html + '</div>').appendTo('body');
          Drupal.dialog(myDialog, {title: 'Message', width: '500px'}).showModal();
        }
      }
    });
  });
})(jQuery);